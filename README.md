General structure of project:

5 subfolders where there is a single subsystem project in each folder.

Start-up in eclipse works the following: 
-clone the git repository: https://gitlab.com/eric186/toy-car-factory
-Use the top directory as the workspace. This should be standardly named toy-car-factory if you havent renamed it manually.
-add all 5 subsystems by pressing new -> project for each subsystem. 
-Instead of naming the projects with a custom name, you insert the axact name of the subsystems folder (for example: "robots" 
	for the  robot arms). You should see if you have entered the right name before pressing next, because a message will
	appear that says that the wizard will import based on the existing source.
-change the jre to the version we have agreed on 1.8.0_191(?).
	
i have also added a folder with useful instructions for working with sockets. An example project and slides from "Softwareentwicklung 2 Praktikum".
	
please do not change any folders in the top directory before asking me first. I still want to be able to run your subsystems without
too much inconvenience :P. Also do not change anything in other peoples subsystems. Git doesn't like that.

