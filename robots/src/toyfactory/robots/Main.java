package toyfactory.robots;


import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import toyfactory.network.data.Command;
import toyfactory.network.data.NetworkCollection;

import java.util.ArrayList;


public class Main extends Application {
    RobotClient client = null;

    @Override
    public void start(Stage primaryStage) throws Exception {

        boolean connectet = true;
        RobotArms[] robotArms = new RobotArms[]{new RobotArms(0, -1, 1, "step0"), new RobotArms(1, 1, 2, "step1"), new RobotArms(2, 2, 3, "step2"), new RobotArms(3, 3, -1, "step3")};
        try {
            client = new RobotClient(NetworkCollection.HOST, NetworkCollection.PORT, robotArms);
        } catch (Exception e) {
            System.out.println("server not found");
            connectet = false;
        }
        final RobotClient c = client;


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        VBox root = new VBox();
        HBox hBoxIDSrart = new HBox();
        HBox hBoxIDold = new HBox();
        HBox hBoxID = new HBox();
        HBox hBoxAddMatrial = new HBox();
        HBox hBoxAddMs = new HBox();

        Text ms = new Text();
        Text id = new Text("0");

        Circle online = new Circle(5, 5, 5);
        if (connectet) {
            online.setFill(Color.GREEN);
        } else {
            online.setFill(Color.RED);
        }
        Circle hasMatrial = new Circle(5, 5, 5);
        hasMatrial.setFill(Color.RED);
        Circle setConveyor = new Circle(5, 5, 5);
        setConveyor.setFill(Color.RED);


        TextField robotStart = new TextField("number of Items");
        TextField robotID = new TextField("1");
        TextField matrialName = new TextField("matrialName");
        TextField matrialNumber = new TextField("1");

        Button buttonSart = new Button("Processing Start");
        Button buttonChangeStore = new Button("Change Store");
        Button buttonChangeRobot = new Button("Change Robot");
        Button buttonGetInventory = new Button("GetInventory");
        Button buttonMartialOnConveyor = new Button("set Conveyor");

        // Clear
        buttonSart.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                RobotArms r = robotArms[0];
                client.start = true;
                for (RobotArms x : robotArms) {
                    x.requestItems=Integer.valueOf(robotStart.getText());
                }
                ms.setText("start processing");
                if (!r.isMatrialInStore) {
                    requestMatrial(r);
                }
            }
        });

        buttonChangeStore.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                for (RobotArms r : robotArms) {
                    if (r.ID == Integer.valueOf(id.getText())) {
                        if (r.stockUp(matrialName.getText(), Integer.valueOf(matrialNumber.getText()))) {
                            ms.setText("Robot: " + r.ID + " add " + matrialName.getText() + " x " + matrialNumber.getText());
                            if (r.isMatrialInStore) {
                                hasMatrial.setFill(Color.GREEN);
                                if (r.isMatrialOnConveyor || r.deliveryConveyor == -1) {
                                    c.processing(r);
                                    if (!r.isMatrialOnConveyor) {
                                        setConveyor.setFill(Color.RED);
                                    }
                                    if (!r.isMatrialInStore) {
                                        hasMatrial.setFill(Color.RED);
                                    }
                                }
                            } else {
                                requestMatrial(r);
                                hasMatrial.setFill(Color.RED);
                            }
                        } else {
                            ms.setText("can not add matrial");
                        }
                    }
                }
            }
        });

        buttonChangeRobot.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                for (RobotArms r : robotArms) {
                    if (r.ID == Integer.valueOf(robotID.getText())) {
                        id.setText(robotID.getText());
                        ms.setText("Interface from Robot: " + r.ID);
                        if (r.isMatrialOnConveyor || r.deliveryConveyor == -1) {
                            setConveyor.setFill(Color.GREEN);
                        } else {
                            setConveyor.setFill(Color.RED);
                        }
                        if (r.isMatrialInStore) {
                            hasMatrial.setFill(Color.GREEN);
                        } else {
                            hasMatrial.setFill(Color.RED);
                        }
                    }
                }
            }
        });

        buttonGetInventory.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                for (RobotArms r : robotArms) {
                    if (r.ID == Integer.valueOf(id.getText())) {
                        ms.setText(r.store.inventory());
                        if (!r.isMatrialInStore) {
                            requestMatrial(r);
                        }
                    }
                }
            }
        });

        buttonMartialOnConveyor.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                for (RobotArms r : robotArms) {
                    if (r.ID == Integer.valueOf(id.getText())) {
                        if (r.isMatrialOnConveyor) {
                            r.isMatrialOnConveyor = false;
                            setConveyor.setFill(Color.RED);
                        } else {
                            r.isMatrialOnConveyor = true;
                            setConveyor.setFill(Color.GREEN);
                            if (r.isMatrialInStore) {
                                c.processing(r);
                                if (!r.isMatrialOnConveyor) {
                                    setConveyor.setFill(Color.RED);
                                }
                                if (!r.isMatrialInStore) {
                                    hasMatrial.setFill(Color.RED);
                                }
                            }
                        }
                        if (r.deliveryConveyor == -1) {
                            r.isMatrialOnConveyor = true;
                            setConveyor.setFill(Color.GREEN);
                            if (r.isMatrialInStore) {
                                c.processing(r);
                            }
                        }
                    }
                }
            }
        });

        hBoxIDSrart.getChildren().addAll(new Text("Number: "),robotStart,buttonSart);
        hBoxIDold.getChildren().addAll(new Text("Robot ID: "), id, new Text(" Online: "), online, new Text(" Store: "), hasMatrial, new Text(" Conveyor: "), setConveyor);
        hBoxID.getChildren().addAll(new Text("ID: "), robotID, buttonChangeRobot);
        hBoxAddMatrial.getChildren().addAll(new Text("Matrial: "), matrialName, new Text("Anzahl: "), matrialNumber, buttonChangeStore);
        hBoxAddMs.getChildren().addAll(ms);

        root.getChildren().addAll(hBoxIDSrart, hBoxIDold, hBoxID, hBoxAddMatrial, buttonMartialOnConveyor, buttonGetInventory, hBoxAddMs);

        Scene scene = new Scene(root, 500, 500);

        primaryStage.setTitle("JavaFX TextField (o7planning.org)");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public void requestMatrial(RobotArms r) {
        ArrayList<String> missingMaterials = r.checkStore();
        if (missingMaterials != null) {
            for (String s : missingMaterials) {
                if (client != null) {
                    client.sendCommand(Command.R_REQMATERIAL, r.ID + ";" + s);
                }
            }
        }
    }


    public static void main(String[] args) {
        launch(args);
    }
}
