package toyfactory.robots;

public class Worktask {
    String worktask = "";
    public Goods[] needMatrial =null;
    int[] needMatrialNumber = null;


    public Worktask(String worktask){
        this.worktask=worktask;
        setNeedMatrials();
    }

    public void setWorktask(String worktask){
        this.worktask=worktask;
        setNeedMatrials();
    }

    public void  setNeedMatrials(){
        switch (worktask){
            case "step0":
                needMatrial = new Goods[]{new Goods("m1",2), new Goods("m2",1),new Goods("finished_Workpiece",50)};
                needMatrialNumber = new int[]{1,5,0};
                break;
            case "step1":
                needMatrial = new Goods[]{new Goods("m3",10)};
                needMatrialNumber = new int[]{10};
                break;
            case "step2":
                needMatrial = new Goods[]{new Goods("m1",6), new Goods("m2",1), new Goods("m3",9)};
                needMatrialNumber = new int[]{6,5,8};
                break;
            case "step3":
                needMatrial = new Goods[]{new Goods("m2",6), new Goods("m3",8),new Goods("finished_Workpiece",50)};
                needMatrialNumber = new int[]{4,2,0};
                break;
            case "standby":
                needMatrial = null;
                needMatrialNumber = null;
                break;
            default:
                break;
        }
    }
}
