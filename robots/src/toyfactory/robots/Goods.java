package toyfactory.robots;

public class Goods {
    public String name;
    public int weights;
    public int numberOf =0;
    public int totalWeight =0;

    public Goods( String name,int weights){
        this.name = name;
        this.weights = weights;
    }

    public boolean addGood(int number){
        if ((numberOf+number)<0){
            return false;
        }
        numberOf = numberOf+number;
        calculatedWeight();
        return true;
    }

    private void calculatedWeight(){
        totalWeight =numberOf*weights;
    }
}
