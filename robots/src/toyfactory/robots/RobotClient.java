package toyfactory.robots;

import toyfactory.network.Client;
import toyfactory.network.data.Command;
import toyfactory.network.data.Subsystem;

import java.io.IOException;
import java.util.ArrayList;

public class RobotClient extends Client {
    boolean start= false;
    boolean connectet = false;
    public RobotArms[] robotArms;
    public RobotClient(String host, int port, RobotArms[] robotArms) throws IOException {
        super(host, port);
        this.robotArms = robotArms;
    }
    @Override
    public Subsystem getSubsystem() {
        return Subsystem.Robot;
    }
    @Override
    public void connectionLost() {
    }

    @Override
    public void receivedCommand(Command cmd) {
        receivedCommand(cmd,null);
    }
    @Override
    public void receivedCommand(Command cmd, String params) {
        System.out.println(cmd.toString()+" , "+ params);
        String[] parts = new String[1];
        if(params!= null){
            parts = params.split(";");
        }
        switch (cmd){
            case CONNECTED:
                connectet = true;
                break;
            case M_R_ADDMATERIAL:
                for(RobotArms r:robotArms){
                    if (r.ID == Integer.valueOf(parts[0])){
                        r.stockUp(parts[1],Integer.valueOf(parts[2]));
                        processing(r);
                    }
                }
                break;
            case M_R_ITEMAT:
                for(RobotArms r:robotArms){
                    if (r.ID == Integer.valueOf(parts[0])){
                        r.isMatrialOnConveyor = true;
                        processing(r);
                    }
                }
                break;
            default:
                System.out.println("Command "+cmd+" not supported!");
                break;
        }
    }

    public void processing(RobotArms r) {
        boolean b = r.processing();
        if(!b){
            ArrayList<String> missingMaterials = r.checkStore();
            if (missingMaterials!=null){
                for (String s:missingMaterials){
                    this.sendCommand(Command.R_REQMATERIAL, r.ID+";"+s);
                }
                if(r.ID==0&&r.store.inventory("finished_Workpiece")==r.requestItems){
                    start = false;
                }
            }
        }

        while (b&&start){
            if (r.sendConveyor != -1){
                this.receivedCommand(Command.M_R_ITEMAT,""+(r.ID+1));
            }
            if(r.deliveryConveyor!= -1){
                this.sendCommand(Command.R_TAKE,""+r.ID+";"+r.deliveryConveyor);
            }
            if(r.sendConveyor!=-1){
                this.sendCommand(Command.R_DELIVER,""+r.ID+";"+r.sendConveyor);
            }else{
            }
            ArrayList<String> missingMaterials = r.checkStore();
            if (missingMaterials!=null){
                for (String s:missingMaterials){
                    this.sendCommand(Command.R_REQMATERIAL, r.ID+";"+s);
                }
                if(r.ID==0&&r.store.inventory("finished_Workpiece")==r.requestItems){
                    start = false;
                }
            }
            b = r.processing();
        }


    }
}
