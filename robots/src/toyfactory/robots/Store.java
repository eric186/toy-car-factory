package toyfactory.robots;

import java.util.HashMap;
import java.util.Iterator;

public class Store {
    HashMap<String,Goods> goods = new HashMap<>();


    public boolean newGoods(String name,int w){
        goods.putIfAbsent(name,new Goods(name,w));
        return true;
    }

    public boolean newGoods(Goods goods){
        this.goods.putIfAbsent(goods.name,goods);
        return true;
    }

    public boolean addGoods(String name,int number){
        if(goods.containsKey(name)){
            if (goods.get(name).addGood(number)) {

                return true;
            }else {
                return false;
            }
        }else {
            return false;
        }
    }

    public int inventory(String name){
        if(goods.containsKey(name)){
            return goods.get(name).numberOf;
        }else {
            return 0;
        }
    }

    public String inventory(){
        StringBuilder inventory = new StringBuilder();
        inventory.append("R: " );
        for (String s:goods.keySet()) {
            inventory.append(goods.get(s).name+" x "+ goods.get(s).numberOf+"\n");
        }
        return inventory.toString();

    }
}
