package toyfactory.robots;

import java.util.ArrayList;

public class RobotArms {
    boolean isMatrialOnConveyor = false;
    boolean isMatrialInStore = false;
    Store store = new Store();
    Worktask worktask;
    int ID,deliveryConveyor,sendConveyor;
    int requestItems;

    public RobotArms(int id,int deliveryConveyor,int sendConveyor, String worktask) {
        this.ID = id;
        this.deliveryConveyor = deliveryConveyor;
        this.sendConveyor = sendConveyor;
        this.worktask = new Worktask(worktask);
        for (Goods g : this.worktask.needMatrial) {
            store.newGoods(g);
        }
    }

    public boolean stockUp(String name, int count) {
        boolean b =store.addGoods(name, count);
        checkStore();
        return b;
    }

    public ArrayList<String> checkStore() {
        ArrayList<String> missingMaterials = new ArrayList<String>();
        isMatrialInStore = true;
        for (int i = 0; i < worktask.needMatrial.length; i++) {
            if (store.goods.get(worktask.needMatrial[i].name).numberOf < worktask.needMatrialNumber[i]) {
                int request = worktask.needMatrialNumber[i]*2;
                missingMaterials.add(""+worktask.needMatrial[i].name+";"+request);
                isMatrialInStore = false;
            }
        }
        return missingMaterials;
    }

    public boolean processing() {
        if(deliveryConveyor ==-1){
            isMatrialOnConveyor =true;
        }
        if (isMatrialOnConveyor && isMatrialInStore) {
            isMatrialOnConveyor = false;
            for (int i = 0; i < worktask.needMatrial.length; i++) {
                store.addGoods(worktask.needMatrial[i].name, worktask.needMatrialNumber[i] * -1);

            }
            if(ID == 0){
                store.addGoods("finished_Workpiece",1);
            }
            return true;
        } else {
            return false;
        }
    }


}
