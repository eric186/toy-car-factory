package toyfactory.warehouse;

import toyfactory.network.data.NetworkCollection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.table.AbstractTableModel;

@SuppressWarnings("serial")
public class Inventory extends AbstractTableModel{
	private volatile List<Site> onlineSites = Collections.synchronizedList(new ArrayList<Site>());
	private volatile List<Site> offlineSites = Collections.synchronizedList(new ArrayList<Site>());
	private static Inventory instance;
	private int nextSiteNr = 0;
	

	
	synchronized public static Inventory getInstance()  
	  { 
	   if(instance == null) {
		   instance = new Inventory();
	   }
	   return instance;
	  } 
	
	
	//implement string representation of stocks
	public String getStock() {
		HashMap<String, Integer> itemMap = new HashMap<String, Integer>();
		Iterator<Site> siteIt = onlineSites.iterator();
		
		while (siteIt.hasNext()){
			List<Item> items = siteIt.next().getItems();
			Iterator<Item> itemIt = items.iterator();
			while(itemIt.hasNext()) {
				Item item = itemIt.next();
				if(itemMap.containsKey(item.getName())) {
					itemMap.put(item.getName(), itemMap.get(item.getName()) + 1);
				}else {
					itemMap.put(item.getName(), 1);
				}
			}
		}
		
		Set<Entry<String, Integer>> entrySet = itemMap.entrySet();
		Iterator<Entry<String, Integer>> entrySetIt = entrySet.iterator();
		String s = new String();
		if(entrySetIt.hasNext()) {
			Entry<String,Integer> entry = entrySetIt.next();
			s = s + entry.getKey() + NetworkCollection.NETWORKDIA + entry.getValue();
			while(entrySetIt.hasNext()) {
				entry = entrySetIt.next();
				s = s + NetworkCollection.NETWORKDIA2 + entry.getKey() + NetworkCollection.NETWORKDIA + entry.getValue();
			}
		}else {
			s = "empty" + NetworkCollection.NETWORKDIA + 0;
		}
		return s;
	}
	
	public List<Site> getOnlineSites() {
		return onlineSites;
	}
	
	
	synchronized public boolean addItem(Item item) {
		Iterator<Site> it = onlineSites.iterator();
		while(it.hasNext()) {
			Site site = it.next();
			if(site.remainingSpace() > 0) {
				site.addItem(item);
				updateCells();
				return true;
			}
		}
		return false;
	}
	
	public boolean removeId(int id) {
		Iterator<Site> it = onlineSites.iterator();
		while(it.hasNext()) {
			Site site = it.next();
			if(site.removeId(id)) {
				updateCells();
				return true;
			}
		}
		return false;
	}
	
	public boolean removeFirstOf(String name) {
		Iterator<Site> it = onlineSites.iterator();
		while(it.hasNext()) {
			Site site = it.next();
			if(site.removeFirstOf(name)) {
				updateCells();
				return true;
			}
		}
		return false;
	}
	
	public void reset() {
		onlineSites = new ArrayList<Site>();
		offlineSites = new ArrayList<Site>();
	}
	
	synchronized public Site addSite() {
		Site site = new Site(nextSiteNr++);
		onlineSites.add(site);
		return site;
	}
	
	public boolean turnOffSite(int siteNr) {
		Iterator<Site> it = onlineSites.iterator();
		while(it.hasNext()) {
			Site site = it.next();
			if(site.getSiteNr() == siteNr) {
				onlineSites.remove(site);
				offlineSites.add(site);
				updateCells();
				return true;
			}
		}
		return false;
	}
	
	public boolean turnOnSite(int siteNr) {
		Iterator<Site> it = offlineSites.iterator();
		while(it.hasNext()) {
			Site site = it.next();
			if(site.getSiteNr() == siteNr) {
				offlineSites.remove(site);
				onlineSites.add(site);
				updateCells();
				return true;
			}
		}
		return false;
	}
	
	public void updateCells() {
		cells = new ArrayList<Object[]>();
		List<Site> siteList = getOnlineSites();
		Iterator<Site> siteIt = siteList.iterator();
		int i = 0;
		while(siteIt.hasNext()) {
			Site currSite = siteIt.next();
			List<Item> itemList= currSite.getItems();
			
			Iterator<Item> itemIt = itemList.iterator();
			while(itemIt.hasNext()) {
				Item item = itemIt.next();
				Object[] obj = new Object[4];
				cells.add(i, obj);
				cells.get(i)[0] = currSite.getSiteNr();
				cells.get(i)[1] = item.getName();
				cells.get(i)[2] = item.getLocalRow();
				cells.get(i)[3] = item.getLocalColumn();
				i++;
			}
		fireTableDataChanged();
		}
	}
	
	//model part of this class
	private ArrayList<Object[]> cells = new ArrayList<Object[]>();
	private final static int COLUMN_COUNT = 4;
	private String[] columnNames = { "Site", "Item Name", "row", "column"};
	
	@Override
	public String getColumnName(int c) {
		return columnNames[c];
	}
	@Override
	public Class<?> getColumnClass(int c) {
		if(cells.size() == 0) {
			return Object.class;
		}
		return cells.get(0)[c].getClass();
	}

	
	@Override
	public int getRowCount() {
		return cells.size();
	}

	@Override
	public int getColumnCount() {
		return COLUMN_COUNT;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return cells.get(rowIndex)[columnIndex];

	}
	
	@Override
	public boolean isCellEditable(int r, int c) {
		return false;
	}
}
