package toyfactory.warehouse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Site {
	private final List<Item> stock = Collections.synchronizedList(new ArrayList<Item>());
	private static int MAX_SPACE = 100; 
	private final int siteNr;
	
	public Site(int siteNr) {
		this.siteNr = siteNr;
	}
	
	public void setSpace(int space){
		MAX_SPACE = space;
	}
	
	public List<Item> getItems() {
		return stock;
	}
	
	public boolean addItem(Item item) {
		if(stock.size() <= 100) {
			stock.add(item);
			return true;
		}else {
			return false;
		}
	}
	
	public boolean removeId(int id) {
		Iterator<Item> it = stock.iterator();
		while(it.hasNext()) {
			Item item = it.next();
			if(item.getId() == id) {
				return stock.remove(item);
			}
		}
		return false;
	}
	
	public boolean removeFirstOf(String name) {
		Iterator<Item> it = stock.iterator();
		while(it.hasNext()) {
			Item item = it.next();
			if(item.getName().equals(name)) {
				return stock.remove(item);
			}
		}
		return false;
	}
	
	
	//searches for low stock of the given item in the stock list
	public boolean checkItemLow(String name){
		Iterator<Item> it = stock.iterator();
		int count = 0;
		while(it.hasNext()) {
			Item item = it.next();
			if(item.getName().equals(name)) {
				count++;
			}
		}
		return count < 10;
	}
	
	public int remainingSpace(){
		return MAX_SPACE - stock.size();
	}
	
	public int getSiteNr() {
		return siteNr;
	}
	
	public String toString() {
		return "Site "+ siteNr;
	}
	
}
