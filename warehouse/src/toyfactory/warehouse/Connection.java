package toyfactory.warehouse;

import java.io.IOException;
import java.net.ConnectException;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import toyfactory.network.Client;
import toyfactory.network.data.Command;
import toyfactory.network.data.NetworkCollection;
import toyfactory.network.data.Subsystem;


public class Connection extends Client{
	
	public Connection(String host, int port) throws IOException {
		super(host, port);
	}

	private final TaskHandler taskHandler = TaskHandler.getInstance(); 
	
	public static void main(String[] args) throws IOException{
		Connection server = null;
		try {
			server = new Connection(NetworkCollection.HOST,NetworkCollection.PORT);
		}catch(ConnectException e) {
			System.out.println("connection not possible");
		}
		Thread displayThread = new Thread(new Display(null));
		displayThread.start();
		if(server != null) {
			server.start();
		}
	}
	
	
	
	private void start() throws IOException {
		System.out.println("Server started");
		System.out.println("Terminate with Enter");
		//program can be exited via the console as well as via the swing application
		System.in.read();
		
		System.out.println("Terminated Server");
		close();
		System.exit(0);
	}	
	

	@Override
	public Subsystem getSubsystem() {
		return Subsystem.Warehouse;
	}

	@Override
	public void connectionLost() {
		close();
	}

	@Override
	public void receivedCommand(Command cmd) {
		System.out.println("Unknown command recieved: " + cmd);
		
	}

	@Override
	public void receivedCommand(Command cmd, String params) {
		if(cmd == Command.M_W_GETSTOCK) {
			sendCommand(Command.W_STOCK, taskHandler.getStock());
		}else if(cmd == Command.M_W_REQMATERIAL) {
			String[] data=params.split(NetworkCollection.NETWORKDIA);
			int IDRobot=Integer.parseInt(data[0]);
			String name=data[1];
			int count=Integer.parseInt(data[2]);
			if(taskHandler.deliver(name, count)) {
				try {
					//wait for a random time to simulate the movement of the forklift
					Random rand = new Random();
					TimeUnit.SECONDS.sleep(rand.nextInt(10) + 5);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				sendCommand(Command.W_DELIVERMATERIAL,IDRobot+NetworkCollection.NETWORKDIA+name+NetworkCollection.NETWORKDIA+count);
			}
		}else if(cmd == Command.M_W_IMPORT) {
			String[] data=params.split(NetworkCollection.NETWORKDIA);
			String name = data[0];
			String stringNr = data[1];
			int count = Integer.parseInt(stringNr);
			taskHandler.pickUp(name, count);
		}else {
			System.out.println("Unknown command recieved: " + cmd + " Params: " + params);
		}
		
	}
}
