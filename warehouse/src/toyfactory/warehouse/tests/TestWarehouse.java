package toyfactory.warehouse.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;

import toyfactory.warehouse.Site;
import toyfactory.warehouse.TaskHandler;

public class TestWarehouse {
	public TaskHandler taskHandler = TaskHandler.getInstance();
	public static final int MAX_SPACE = 100;
	
	@Rule public TestName name = new TestName();
	
	@Before
	public void init() {
		taskHandler.resetStock();
	}
	
	@Test
	public void testGetStock() {
		TaskHandler taskHandler = TaskHandler.getInstance();
		
		taskHandler.addSite();
		
		assertEquals(true, taskHandler.addItem("test", 0, 0));
		assertEquals(true, taskHandler.addItem("test", 0, 1));
		assertEquals(true, taskHandler.addItem("test1", 0, 2));
		
		String expected = "test;2;;test1;1";

		assertEquals(expected, taskHandler.getStock());
		
		System.out.println("|-------------------------------------------|");
		System.out.println("Test: " + name.getMethodName());
		System.out.println("expected: "+ expected +" actual: " + taskHandler.getStock());

	}
	
	@Test
	public void testaddItems() {
		TaskHandler taskHandler = TaskHandler.getInstance();
		
		Site site = taskHandler.addSite();
		site.setSpace(3);
		Site site2 = taskHandler.addSite();
		site2.setSpace(3);
		
		
		assertEquals(true, taskHandler.addItem("test", 0, 0));
		assertEquals(true, taskHandler.addItem("test", 0, 1));
		assertEquals(true, taskHandler.addItem("test1", 0, 2));
		assertEquals(true, taskHandler.addItem("test", 0, 0));
		assertEquals(true, taskHandler.addItem("test", 0, 1));
		assertEquals(true, taskHandler.addItem("test1", 0, 2));
		//last item should not be added because the sites are full;
		assertEquals(false, taskHandler.addItem("test1", 0, 2));
		
		String expected = "test;4;;test1;2";
		
		assertEquals(expected, taskHandler.getStock());
		
		System.out.println("|-------------------------------------------|");
		System.out.println("Test: " + name.getMethodName());
		System.out.println("expected: "+ expected +" actual: " + taskHandler.getStock());
		site.setSpace(MAX_SPACE);
		site2.setSpace(MAX_SPACE);
	}
	
	@Test
	public void testReset() {
		TaskHandler taskHandler = TaskHandler.getInstance();
		
		taskHandler.addSite();
		taskHandler.addSite();
		
		assertEquals(true, taskHandler.addItem("test", 0, 0));
		assertEquals(true, taskHandler.addItem("test", 0, 1));
		assertEquals(true, taskHandler.addItem("test1", 0, 2));
		assertEquals(true, taskHandler.addItem("test1", 0, 2));
		
		taskHandler.resetStock();
		
		String expected = "";
		
		assertEquals(expected, taskHandler.getStock());
		System.out.println("|-------------------------------------------|");
		System.out.println("Test: " + name.getMethodName());
		System.out.println("expected: "+ expected +" actual: " + taskHandler.getStock());
	}
	
	@Test
	public void testValidDeliver() {
		TaskHandler taskHandler = TaskHandler.getInstance();
		
		taskHandler.addSite();
		taskHandler.addSite();
		
		assertEquals(true, taskHandler.addItem("test", 0, 0));
		assertEquals(true, taskHandler.addItem("test", 0, 1));
		assertEquals(true, taskHandler.addItem("test1", 0, 2));
		assertEquals(true, taskHandler.addItem("test1", 0, 2));
		
		assertEquals(true, taskHandler.deliver("test", 2));
		
		String expected = "test1;2";
		
		assertEquals(expected, taskHandler.getStock());
		System.out.println("|-------------------------------------------|");
		System.out.println("Test: " + name.getMethodName());
		System.out.println("expected: "+ expected +" actual: " + taskHandler.getStock());
	}
	
	@Test
	public void testInvalidDeliver() {
		TaskHandler taskHandler = TaskHandler.getInstance();
		
		taskHandler.addSite();
		taskHandler.addSite();
		
		assertEquals(true, taskHandler.addItem("test", 0, 0));
		assertEquals(true, taskHandler.addItem("test", 0, 1));
		assertEquals(true, taskHandler.addItem("test1", 0, 2));
		assertEquals(true, taskHandler.addItem("test1", 0, 2));
		
		assertEquals(false, taskHandler.deliver("test2", 3));
		
		String expected = "test;2;;test1;2";
		assertEquals(expected, taskHandler.getStock());
		System.out.println("|-------------------------------------------|");
		System.out.println("Test: " + name.getMethodName());
		System.out.println("expected: "+ expected +" actual: " + taskHandler.getStock());
	}
	
	@Test
	//this test takes a little longer due to the design of the pickUp method
	public void testPickUp() {
		TaskHandler taskHandler = TaskHandler.getInstance();
		
		taskHandler.addSite();
		
		taskHandler.pickUp("test", 1);
		taskHandler.pickUp("test1", 2);
		taskHandler.pickUp("test", 3);
		
		String expected = "test;4;;test1;2";
		assertEquals(expected, taskHandler.getStock());
		System.out.println("|-------------------------------------------|");
		System.out.println("Test: " + name.getMethodName());
		System.out.println("expected: "+ expected +" actual: " + taskHandler.getStock());
	}
	
	
	
	
}
