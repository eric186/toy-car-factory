package toyfactory.warehouse;


//a class to store the location of each item 
public class Location {

	private int row;
	private int column;
	
	public Location(int row, int column) {
		this.row = row;
		this.column = column;
	}
	
	public int getRow() {
		return row;
	}
	
	public int getColumn() {
		return column;
	}
}
