package toyfactory.warehouse;

public class Item {
	private int id;
	private String name;
	private Location location;
	private static volatile int nextId = 0;
	
	public Item(String name) {
		synchronized(this) {
			this.id = nextId++;
		}
		this.name = name;
	}
	
	public int getLocalRow() {
		return location.getRow();
	}
	
	public int getLocalColumn() {
		return location.getColumn();
	}
	
	public void updateLocation(int row, int column) {
		location = new Location(row, column);
	}
	
	public String getName() {
		return name;
	}
	
	public int getId() {
		return id;
	}
}
