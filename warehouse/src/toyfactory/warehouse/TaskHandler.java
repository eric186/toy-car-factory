package toyfactory.warehouse;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class TaskHandler {
	//may have to be volatile
	private boolean isStopped;
	private static TaskHandler instance;
	private Inventory inv = Inventory.getInstance();
	
	synchronized public static TaskHandler getInstance()  
	  { 
	    if (instance == null)  
	    { 
	      // if instance is null, initialize 
	      instance = new TaskHandler(); 
	    }
	    return instance; 
	  } 
	
	public boolean deliver(String name, int nr) {
		if(!isStopped) {
			for(int i = 0; i < nr; i++) {
				if(!inv.removeFirstOf(name)) {
					return false;
				}
			}
			return true;
		}
		return false;
	}
	
	public void stop(boolean stopped) {
		isStopped = stopped;
	}
	
	public void turnOffSite(int nr) {
		if(!isStopped) {
			inv.turnOffSite(nr);
		}
	}
	
	public void turnOnSite(int nr) {
		if(!isStopped) {
			inv.turnOnSite(nr);
		}
	}
	
	public void pickUp(String name, int nr) {
		//Normally we would send a forklift to pick those items up at the import zone. RIght now we simply add the items after a random time
		try {
			//wait for a random time to simulate the movement of the forklift
			Random rand = new Random();
			TimeUnit.SECONDS.sleep(rand.nextInt(10) + 5);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		for(int i = 0; i < nr; i++) {
			addItem(name, 0, 0);
		}
	}
	
	public boolean addItem(String name, int row, int column) {
		Item item = new Item(name);
		item.updateLocation(row, column);
		return inv.addItem(item);
	}
	
	public Site addSite() {
		return inv.addSite();
	}
	
	public String getStock() {
		return inv.getStock();
	}
	
	public void resetStock() {
		inv.reset();
	}
	
}
