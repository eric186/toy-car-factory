package toyfactory.warehouse;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import net.miginfocom.swing.MigLayout;

public class Display implements Runnable{
	private JFrame frame;
	private JPanel panel;
	private JButton addItemButton;
	private JButton addSiteButton;
	private JTextField addTextField;
	private JTextField locationTextFieldX;
	private JTextField locationTextFieldY;
	private TaskHandler taskHandler = TaskHandler.getInstance();
	private JTable onlineTable;
	private JPanel onlineTablePanel;
	private JList<String> onlineSiteList;
	private JList<String> offlineSiteList;
	private DefaultListModel<String> onlineModel = new DefaultListModel<String>();
	private DefaultListModel<String> offlineModel = new DefaultListModel<String>();
	private JButton setOfflineButton;
	private JButton setOnlineButton;
	private Connection con;
	
	public Display(Connection con) {
		this.con = con;
	}
	
	@Override
	public void run() {
		Display disp = new Display(con);
		disp.start();
	}
	
	public void start() {
		init();
		
	}
	
	public void init() {
		frame = new JFrame("Warehouse");
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
            	if(con != null) {
            		con.close();
            	}
            }
        });
		panel = new JPanel(new MigLayout());
		frame.getContentPane().add(panel);
		
		
		onlineSiteList = new JList<String>(onlineModel);
		offlineSiteList = new JList<String>(offlineModel);
		JPanel onlineSitePanel = new JPanel(new MigLayout());
		JPanel offlineSitePanel = new JPanel(new MigLayout());
		JScrollPane onlineScroll = new JScrollPane(onlineSiteList);
		JScrollPane offlineScroll = new JScrollPane(offlineSiteList);
		onlineScroll.setPreferredSize(new Dimension(100, 400));
		offlineScroll.setPreferredSize(new Dimension(100, 400));
		onlineSitePanel.add(onlineScroll);
		offlineSitePanel.add(offlineScroll);
		
		
		setOfflineButton = new JButton("set site offline");
		setOfflineButton.setPreferredSize(new Dimension(100, 25));
		setOfflineButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int idx = onlineSiteList.getSelectedIndex();
				String s = onlineSiteList.getSelectedValue();
				if(s != null) {
					char c = s.charAt(s.length()-1);
					int nr = Integer.parseInt(Character.toString(c));
					taskHandler.turnOffSite(nr);
					onlineModel.remove(idx);
					s = s.substring(8);
					offlineModel.addElement("offline: " + s);
				}
								
			}
			
		});
		
		setOnlineButton = new JButton("set site online");
		setOnlineButton.setPreferredSize(new Dimension(100, 25));
		setOnlineButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int idx = offlineSiteList.getSelectedIndex();
				String s = offlineSiteList.getSelectedValue();
				if(s != null) {
					char c = s.charAt(s.length()-1);
					int nr = Integer.parseInt(Character.toString(c));
					taskHandler.turnOnSite(nr);
					offlineModel.remove(idx);
					s = s.substring(9);
					onlineModel.addElement("online: " + s);
				}
				
			}
			
		});
		
		
		
		
		addItemButton = new JButton("add Item");
		addItemButton.setPreferredSize(new Dimension(100, 25));
		addItemButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					boolean successful;
					successful = taskHandler.addItem(addTextField.getText(), Integer.parseInt(locationTextFieldX.getText()), Integer.parseInt(locationTextFieldY.getText()));
					if(!successful) {
						JOptionPane.showMessageDialog(frame, "Something went wrong when adding a new item. You might not have added any sites or all the sites are full");
					}
					
				}catch(Exception ex) {
					ex.printStackTrace();
				}
				
			}
			
		});
		
		addSiteButton = new JButton("add new Sites");
		addSiteButton.setPreferredSize(new Dimension(100, 25));

		addSiteButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					Site site = taskHandler.addSite();
					onlineModel.addElement("online: " + site.toString());
				}catch(Exception ex) {
					ex.printStackTrace();
				}
				
			}
			
		});
		
		addTextField = new JTextField();
		addTextField.setPreferredSize(new Dimension(100, 25));
		locationTextFieldX = new JTextField();
		locationTextFieldX.setPreferredSize(new Dimension(50, 25));
		
		locationTextFieldY = new JTextField();
		locationTextFieldY.setPreferredSize(new Dimension(50, 25));
		
		onlineTable = new JTable();
		onlineTablePanel = new JPanel(new MigLayout());
		onlineTablePanel.add(new JScrollPane(onlineTable));
		onlineTablePanel.setPreferredSize(new Dimension(400,400));
		onlineTable.setModel(Inventory.getInstance());
		
		
		frame.setSize(1200, 800);
		panel.add(addTextField);
		panel.add(locationTextFieldX);
		panel.add(locationTextFieldY);
		panel.add(addItemButton);
		panel.add(addSiteButton, "wrap");
		panel.add(onlineTablePanel, "span 4");
		panel.add(onlineSitePanel);
		panel.add(offlineSitePanel);
		panel.add(setOfflineButton);
		panel.add(setOnlineButton);
		
		
		frame.setVisible(true);
		
		
	}
	
}
