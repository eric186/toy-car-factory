package toyfactory.warehouse;

import javax.swing.table.AbstractTableModel;

@SuppressWarnings("serial")
public class InventoryTableModel extends AbstractTableModel{

	private Object[][] cells = {
		{1, "engine", 0, 1},
		{2, "tyre",  1, 0}
	};
	private String[] columnNames = { "Site", "Item Name", "row", "column"};
	
	@Override
	public String getColumnName(int c) {
		return columnNames[c];
	}
	@Override
	public Class<?> getColumnClass(int c) {
		return cells[0][c].getClass();
	}

	
	@Override
	public int getRowCount() {
		return cells.length;
	}

	@Override
	public int getColumnCount() {
		return cells[0].length;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return cells[rowIndex][columnIndex];

	}
	
	@Override
	public boolean isCellEditable(int r, int c) {
		return false;
	}
}
