package toyfactory.network.data;

import java.util.regex.Pattern;

public class NetworkCollection {
    public static Pattern NetworkMessageReadPattern=Pattern.compile("(?<sender>.*?):(?<command>.*?)(?::(?<params>.*))?$");
    public static int PORT=13370;
    public static String HOST="localhost";
    public static String NETWORKDIA=";";
    public static String NETWORKDIA2=";;";
}
