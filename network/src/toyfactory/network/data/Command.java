package toyfactory.network.data;

public enum Command {
    CONNECTED,CLOSE,
    /* conveyor to monitor */                  C_LUBRICANT_NEEDED /* ID of conveyor*/, C_EMERGENCY, C_ITEMAT /* ID robot (send to robot) */, C_SPEED /* ID of conveyor, Speed (double 0<x<3) | id, speed ... */, C_NOITEM /* ID of conveyor */, C_STATE /* ID conveyor, FULL/EMPTY*/,
    /* Warehouse to monitor */                 W_STOCK /* name, count | name, count ... */, W_DELIVERMATERIAL /* ID robot, name, count */,
    /* robots to monitor */                    R_REQMATERIAL /* ID of robot, name, count */, R_CHECKCONVEYOR /* ID robot, ID conveyor */, R_DELIVER /* ID robot, ID conveyor*/, R_TAKE /* ID robot, ID conveyor */,
    /* monitor to conveyor */      M_C_GETSPEED, M_C_ITEMPUT /* ID of conveyor, Speed (double 0<x<3) (if robot lays item) */, M_C_SETSPEED /* ID of conveyor, SPEED (double 0<x<3) */, M_C_ISITEMAT /* ID of Conveyor */, M_C_ITEMTAKE /* ID conveyor */,
    /* monitor to robots */         M_R_ADDMATERIAL /* ID robot, name, count */, M_R_ITEMAT /* ID robot */,
    /* monitor to warehouse */     M_W_GETSTOCK, M_W_REQMATERIAL /* ID of robot, name, count */, M_W_IMPORT /* name, count */
}
