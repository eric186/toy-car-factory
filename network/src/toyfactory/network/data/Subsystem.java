package toyfactory.network.data;

public enum Subsystem {
    Warehouse("W"), Robot("R"), Conveyor("C"),MonitoringSystem("M");

    private String shortName;
    Subsystem(String shortName){
        this.shortName=shortName;
    }
    @Override
    public String toString(){
        return shortName;
    }
}
