package toyfactory.network;

import toyfactory.network.data.Command;
import toyfactory.network.data.NetworkCollection;
import toyfactory.network.data.Subsystem;

import java.io.*;
import java.net.Socket;
import java.util.regex.Matcher;

public abstract class Client{
    private Thread thread;
    private BufferedWriter bw;
    private BufferedReader br;
    private Socket socket;
    private boolean closed=false;
    public Client(String host, int port) throws IOException{
        this(new Socket(host,port));
    }
    public Client(Socket socket) throws IOException{
        this.socket=socket;
        bw=new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        br=new BufferedReader(new InputStreamReader(socket.getInputStream()));
        sendCommand(Command.CONNECTED,getSubsystem().name());
        thread=new Thread(() -> {
            do{
                try{
                    String msg=br.readLine();
                    if(msg!=null) {
                        Matcher m = NetworkCollection.NetworkMessageReadPattern.matcher(msg);
                        while (m.find())
                            if (m.groupCount() == 2)
                                if(Command.valueOf(m.group("command"))== Command.CLOSE) {
                                    close();
                                    connectionLost();
                                }else
                                    receivedCommand(Command.valueOf(m.group("command")));
                            else
                                receivedCommand(Command.valueOf(m.group("command")), m.group("params"));
                    }
                }catch(IOException e){
                    connectionLost();
                }
            }while(!Thread.interrupted());
        });
        thread.setDaemon(true);
        thread.start();
    }
    public abstract Subsystem getSubsystem();
    public abstract void connectionLost();
    public void close(){
        if(!closed){
            closed=true;
            thread.interrupt();
            try{
                sendCommand(Command.CLOSE);
                bw.close();
                br.close();
                socket.close();
            }catch(IOException ignored){}
        }
    }
    public void sendCommand(Command cmd){
        sendCommand(cmd,null);
    }
    public void sendCommand(Command cmd, String params){
        try{
            bw.write(getSubsystem()+":"+cmd+(params==null?"":":"+params)+"\n");
            bw.flush();
        }catch (IOException e){
            connectionLost();
        }
    }
    public abstract void receivedCommand(Command cmd);
    public abstract void receivedCommand(Command cmd, String params);
}
