package sel;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class Globals {
	
	public final static boolean DEBUG = true; 
	public final static int PORT = 8888; 
	public final static int BUFFER_SIZE = 64; 
	public final static String SEPARATOR = "\n";

	public static String readInput(SocketChannel channel, ByteBuffer buffer) throws IOException {
		buffer.clear(); 
		channel.read(buffer); 
		buffer.flip(); 
		byte[] data = new byte[buffer.limit()]; 
		buffer.get(data); 
		String msg = new String(data).trim(); 
		if (DEBUG) System.out.format("<-- received %s%n",msg);
		return msg; 
	}
	
	public static int writeMsg(SocketChannel channel, ByteBuffer buffer, String msg) throws IOException {
		buffer.clear(); 
		buffer.put((msg + SEPARATOR).getBytes()); 
		buffer.flip(); 
		int n = channel.write(buffer); 
		if (DEBUG) System.out.format("--> sent %s%n", msg);
		return n; 
	}

}
