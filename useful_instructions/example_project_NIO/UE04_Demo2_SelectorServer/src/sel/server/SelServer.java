package sel.server;

import static sel.Globals.BUFFER_SIZE;
import static sel.Globals.PORT;
import static sel.Globals.readInput;
import static sel.Globals.writeMsg;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedSelectorException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

import inout.In;
import inout.Out;

public class SelServer {
	
	private volatile boolean terminate = false; 
	private Thread selectorThread; 
	private Selector selector; 
	private ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE); 

	public static void main(String[] args) throws IOException {
		SelServer server = new SelServer(); 
		server.start(); 
	}

	private void start() throws IOException {
		
		buffer = ByteBuffer.allocate(BUFFER_SIZE); 
		
		selector = Selector.open();
	
		ServerSocketChannel serverChannel = ServerSocketChannel.open();
		serverChannel.bind(new InetSocketAddress(PORT));
		serverChannel.configureBlocking(false); 
		serverChannel.register(selector, SelectionKey.OP_ACCEPT); 
		
		selectorThread = new Thread(new SelectorRunnable()); 
		selectorThread.start();

		Out.println("Server started");
		
		Out.println("Terminate with ENTER");
		In.readLine(); 
		terminate();  
		try {
			selectorThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Out.println("Terminated server");
	}
	
	class SelectorRunnable implements Runnable {

		@Override
		public void run() {
			
			while (! terminate) {
				try {
	
					@SuppressWarnings("unused")
					int n = selector.select(2000);  // blocking with timeout for termination 
					
					Set<SelectionKey> keys = selector.selectedKeys(); 
					Iterator<SelectionKey> keyIt = keys.iterator(); 
					
					while (keyIt.hasNext()) {
						SelectionKey key = keyIt.next(); 
						if (key.isAcceptable()) {
							ServerSocketChannel serverSocket = (ServerSocketChannel)key.channel();
							SocketChannel clientChannel = serverSocket.accept();
					        clientChannel.configureBlocking(false);
					        @SuppressWarnings("unused")
							SelectionKey selKey = clientChannel.register(selector, SelectionKey.OP_READ); 
						} else if (key.isReadable()) {
							SocketChannel chnl = (SocketChannel)key.channel(); 
							String msg = readInput(chnl, buffer); 
							if (msg.startsWith("END")) {
								writeMsg(chnl, buffer, msg); 
								chnl.close();
							} else {
								// multiple messages to client !!!!
								writeMsg(chnl, buffer, msg); 
								writeMsg(chnl, buffer, msg); 
								writeMsg(chnl, buffer, msg); 
								writeMsg(chnl, buffer, msg); 
							}
						}
						keyIt.remove(); 
					}
				} catch (ClosedSelectorException | IOException e) {
					e.printStackTrace();
				}
			}
		}

	}

	public void terminate() {
		terminate = true; 		
	}

}