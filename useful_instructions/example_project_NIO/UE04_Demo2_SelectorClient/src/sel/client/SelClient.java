package sel.client;

import static sel.Globals.*;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

import inout.In;
import inout.Out;

public class SelClient {
	
	public static void main(String[] args) throws IOException {
		SelClient client = new SelClient(); 
		client.start(); 
	}

	private volatile boolean terminate = false; 
	private ByteBuffer outBuffer;
	private SocketChannel channel;
	private Thread inputThread; 
	private String name; 
	
	SelClient() {
		inputThread = new Thread(new InputRunnable());
	}

	private void start() {
		
		name = readUserInput("Insert your name: ");; 
				
		outBuffer = ByteBuffer.allocate(BUFFER_SIZE); 
		try {
			channel = SocketChannel.open();
			channel.connect(new InetSocketAddress("localhost", PORT));
			
			inputThread.start();

			writeMsg(channel, outBuffer, "LOGIN " + name); 
			Out.println("\nInsert commands, terminate with END \n"); 
			String cmd = readUserInput("Next command: "); 
			while (!cmd.startsWith("END")) {
				writeMsg(channel, outBuffer, cmd); 
				cmd = readUserInput("Next command: ");
			}
			writeMsg(channel, outBuffer, "END"); 
			
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}

	private String readUserInput(String prompt) {
		Out.println(prompt); 
		return In.readLine().toUpperCase();
	}
	
	private class InputRunnable implements Runnable {
		
		private String input = ""; 
		private ByteBuffer inBuffer = ByteBuffer.allocate(BUFFER_SIZE); 

		@Override
		public void run() {
			while (!terminate) {
				try {
					input = readInput(channel, inBuffer); 
					// TODO: handle multiple and possible incomplete messages  
					Out.println("                Received input: " + input); 
					if (input.startsWith("END")) {
						terminate(); 
					}
				} catch (IOException e) {
				} 
			}
			try {
				channel.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			Out.println("\nClient terminated! \n");
		}

		private void terminate() {
			terminate = true; 
		}
		
	}
}
