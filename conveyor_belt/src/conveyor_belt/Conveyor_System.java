package conveyor_belt;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import toyfactory.network.data.NetworkCollection;

import java.net.InetSocketAddress;

public class Conveyor_System { 
	static Conveyor_belt section1 = new Conveyor_belt(1);
	static Conveyor_belt section2 = new Conveyor_belt(2);
	static Conveyor_belt section3 = new Conveyor_belt(3);
	static Conveyor_belt[] sectionArray = {section1, section2, section3};
	static Communication com;
	
	public static void main(String[] args) throws IOException{
		Lubrication_sensor ls = new Lubrication_sensor();
		ls.start();
		Emergency_stop_button esb = new Emergency_stop_button();
		esb.start();
		com = new Communication(NetworkCollection.HOST, NetworkCollection.PORT);
		
		
		System.out.println("Server started");
		System.out.println("Terminate with Enter");
		//program can be exited via the console as well as via the swing application
		System.in.read();
		
		System.out.println("Terminated Server");
		com.close();
		System.exit(0);
	}
	
	public static void estop() {
		section1.speed = 0;
		section2.speed = 0;
		section3.speed = 0;
		com.C_EMERGENCY();
	}
	
	public static void changespeed(double speed, int id) {
		Conveyor_belt section = sectionArray[id-1];
		if (section.speed==0) {
			if (Item_sensor.checkformaterial(section)) {
				section.speed = speed;
				section.start();
			}else {
				com.NOITEM(id);
			}
		}else {
			if (Speed_sensor.checkforspeed(section)==speed) {
				
			}else{
				section.speed = speed;
			}
		}
	}
	
	public static void lubeneeded(int id) {
		com.C_LUBRICANT_NEEDED(id);
	}
	
	public static void material(int id) {
		Conveyor_belt section = sectionArray[id-1];
		section.material=true;
	}
	
	public static void removed(int id) {
		Conveyor_belt section = sectionArray[id-1];
		section.material=false;
	}
	
	public static void delivered(int id) {
		com.C_ITEMAT(id);
	}
	
	public static void checkitem(int id) {
		Conveyor_belt section = sectionArray[id-1];
		String output= Integer.toString(id)+","; 
		if (Item_sensor.checkformaterial(section)) {
			com.C_STATE(output+"FULL");
		}else {
			com.C_STATE(output+"EMPTY");
		}
	}
}