package conveyor_belt;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;

import javax.swing.JButton;
import javax.swing.JFrame;



public class Emergency_stop_button {
private Thread stopThread;
	
	void start() {
		stopThread = new Thread (new emergencyRunnable());
		stopThread.setDaemon(true);
		stopThread.start();
	}
	
	class emergencyRunnable implements Runnable{
		
		JFrame frame = new JFrame();
		boolean pressed = false;
		
		@Override
		public void run() {
			JButton button = new JButton("Emergency");
			button.setFont(new Font("Arial", Font.PLAIN, 40));
			button.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					if(!pressed) {
						Conveyor_System.estop();
					}
					pressed = true;
				}
				
			});
			
			
			frame.setSize(600, 200);
			frame.add(button);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setVisible(true);
			
		}
	}
}
