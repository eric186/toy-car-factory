package conveyor_belt;

import conveyor_belt.Lubrication_sensor.LubricationRunnable;

public class Conveyor_belt {
	public int id;
	public double speed = 0;
	public boolean material = false;
	private Thread moveThread;
	
	public Conveyor_belt(int id) {
		this.id=id;
	}
	
	
	void start() {
		moveThread = new Thread (new MoveRunnable());
		moveThread.start();
	}
	
	class MoveRunnable implements Runnable{
		@Override
		public void run() {
			long time = -System.currentTimeMillis();
			while (speed!=0) {
					try {
						Thread.sleep(500);
						if((time - System.currentTimeMillis()) >= 2/speed) {
							speed = 0;
							Conveyor_System.delivered(id);
						}
					} catch (InterruptedException e) {
						e.printStackTrace();
					}				
			}
		}
	}
}
