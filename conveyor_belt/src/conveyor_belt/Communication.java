package conveyor_belt;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import toyfactory.network.Client;
import toyfactory.network.data.Command;
import toyfactory.network.data.Subsystem;



public class Communication extends Client{

	public Communication(String host, int port) throws IOException {
		super(host, port);
	}

	@Override
	public Subsystem getSubsystem() {
		return Subsystem.Conveyor;
	}

	@Override
	public void connectionLost() {
		close();
		
	}

	@Override
	public void receivedCommand(Command cmd) {
		if(cmd == Command.M_C_GETSPEED) {
			String output= "";
			for(int i = 0;i < Conveyor_System.sectionArray.length; i++) {
				output += Integer.toString(i+1);
				output += ",";
				output += Double.toString(Conveyor_System.sectionArray[i].speed);
			}	
			C_SPEED(output);
		}
	}

	@Override
	public void receivedCommand(Command cmd, String params) {
		if(cmd == Command.M_C_ITEMPUT) {
			String[] data = params.split(";");
			int id = Integer.parseInt(data[0]);
			double speed = Double.parseDouble(data[1]);
			Conveyor_System.material(id);
			Conveyor_System.changespeed(speed, id);
			
		}else if(cmd == Command.M_C_SETSPEED) {
			String[] data = params.split(";");
			int id = Integer.parseInt(data[0]);
			double speed = Double.parseDouble(data[1]);
			Conveyor_System.changespeed(speed, id);
			
		}else if (cmd == Command.M_C_ISITEMAT) {
			int id = Integer.parseInt(params);
			Conveyor_System.checkitem(id);
			
		}else if(cmd == Command.M_C_ITEMTAKE) {
			int id = Integer.parseInt(params);
			Conveyor_System.removed(id);
		}
	}
	public void NOITEM(int id) {
		String Nr = Integer.toString(id);
		sendCommand(Command.C_NOITEM, Nr);
	}
	
	public void C_LUBRICANT_NEEDED(int id) {
		String Nr = Integer.toString(id);
		sendCommand(Command.C_LUBRICANT_NEEDED, Nr);
	}
	
	public void C_EMERGENCY() {
		sendCommand(Command.C_EMERGENCY);
	}
	
	public void C_ITEMAT(int id) {
		String robot_id = Integer.toString(id+1);
		sendCommand(Command.C_ITEMAT, robot_id);
	}
	
	public void C_SPEED(String output) {
		sendCommand(Command.C_SPEED, output);
	}
	
	public void C_STATE(String output) {
		sendCommand(Command.C_STATE, output);
	}
}