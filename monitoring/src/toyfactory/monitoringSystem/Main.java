package toyfactory.monitoringSystem;

import javafx.application.Application;
import toyfactory.monitoringSystem.model.Model;
import toyfactory.monitoringSystem.model.logger.Logger;
import toyfactory.monitoringSystem.view.MainView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Main{
    public static void main(String[] args) throws IOException {
        new Model();
        if(Arrays.stream(args).anyMatch(s->s.equals("--silent"))){
            System.out.println("Use exit to close!");
            BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
            Logger.addListener((subsystem, message) -> System.out.println(subsystem+": "+message));
            //noinspection StatementWithEmptyBody
            while(!br.readLine().equals("exit")){}
            br.close();
        }else
            Application.launch(MainView.class);
    }
}
