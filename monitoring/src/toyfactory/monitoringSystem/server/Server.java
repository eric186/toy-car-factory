package toyfactory.monitoringSystem.server;

import javafx.collections.FXCollections;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableMap;
import toyfactory.monitoringSystem.model.connectors.ConveyorConnector;
import toyfactory.monitoringSystem.model.connectors.RobotConnector;
import toyfactory.monitoringSystem.model.connectors.WarehouseConnector;
import toyfactory.monitoringSystem.model.structures.WarehouseItem;
import toyfactory.network.data.Command;
import toyfactory.network.data.NetworkCollection;
import toyfactory.network.data.Subsystem;

import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Server{
    private Thread thread;
    private ServerSocket socket;
    private ObservableMap<Subsystem,ServerClient> clients;
    public Server(int port){
        clients=FXCollections.observableMap(new HashMap<>());
        try {
            socket=new ServerSocket(port);
            thread=new Thread(()->{
                do{
                    try{
                        new ServerClient(socket.accept(),this);
                    }catch(IOException ignored){}
                }while(!Thread.interrupted());
                close();
            });
            thread.setDaemon(true);
            thread.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public ArrayList<InetAddress> getIP(){
        try{
            return Collections.list(NetworkInterface.getNetworkInterfaces()).stream()
                    .filter(n->n.getInterfaceAddresses().size()>0&&n.getInterfaceAddresses().stream().anyMatch(i->i.getAddress().getHostAddress().contains(".")))
                    .map(networkInterface -> networkInterface.getInterfaceAddresses().stream().filter(i->i.getAddress().getHostAddress().contains(".")).map(InterfaceAddress::getAddress).collect(Collectors.toList()))
                    .collect(Collector.of((Supplier<ArrayList<InetAddress>>) ArrayList::new, ArrayList::addAll,(r, o)->{r.addAll(o);return r;},(r)->r));
        }catch(SocketException e){return new ArrayList<>();}
    }
    @SuppressWarnings("WeakerAccess")
    public void close(){
        try {
            thread.interrupt();
            clients.values().forEach(ServerClient::close);
            socket.close();
        }catch(IOException ignored){}
    }
    @SuppressWarnings("WeakerAccess")
    public void received(Subsystem subsystem, Command cmd,String parameter){
        String[] data;
        if(cmd==Command.CLOSE)
            clients.remove(subsystem).close();
        switch(subsystem){
            case Conveyor:
                switch(cmd){
                    case C_ITEMAT:
                        ConveyorConnector.conveyor_itemAvailable(Integer.parseInt(parameter));
                        break;
                    case C_NOITEM:
                        ConveyorConnector.conveyor_noItemAt(Integer.parseInt(parameter));
                        break;
                    case C_EMERGENCY:
                        ConveyorConnector.conveyor_emergencyPressed();
                        break;
                    case C_LUBRICANT_NEEDED:
                        ConveyorConnector.conveyor_needLubricant(Integer.parseInt(parameter));
                        break;
                    case C_SPEED:
                        ConveyorConnector.conveyor_speedOfConveyor(Arrays.stream(parameter.split(NetworkCollection.NETWORKDIA2)).map(s -> {
                            String[] item=s.split(NetworkCollection.NETWORKDIA);
                            return new ConveyorConnector.ConveyorSpeed(Integer.parseInt(item[0]),Double.parseDouble(item[1]));
                        }).collect(Collectors.toList()));
                        break;
                    case C_STATE:
                        data=parameter.split(NetworkCollection.NETWORKDIA);
                        ConveyorConnector.conveyor_state(Integer.parseInt(data[0]), data[1].equals("FULL"));
                        break;
                    default:
                        System.out.println("Not Supported Command: "+cmd+" -> "+parameter);
                }
                break;
            case Robot:
                switch(cmd){
                    case R_CHECKCONVEYOR:
                        data=parameter.split(NetworkCollection.NETWORKDIA);
                        RobotConnector.robot_checkConveyorState(Integer.parseInt(data[0]),Integer.parseInt(data[1]));
                        break;
                    case R_DELIVER:
                        data=parameter.split(NetworkCollection.NETWORKDIA);
                        RobotConnector.robot_deliverAtConveyor(Integer.parseInt(data[0]),Integer.parseInt(data[1]));
                        break;
                    case R_REQMATERIAL:
                        data=parameter.split(NetworkCollection.NETWORKDIA);
                        RobotConnector.robot_requestMaterialWarehouse(Integer.parseInt(data[0]),data[1],Integer.parseInt(data[2]));
                        break;
                    case R_TAKE:
                        data=parameter.split(NetworkCollection.NETWORKDIA);
                        RobotConnector.robot_takeOfConveyor(Integer.parseInt(data[0]),Integer.parseInt(data[1]));
                        break;
                    default:
                        System.out.println("Not Supported Command: "+cmd+" -> "+parameter);
                }
                break;
            case Warehouse:
                switch(cmd){
                    case W_STOCK:
                        WarehouseConnector.warehouse_currentStock(Arrays.stream(parameter.split(NetworkCollection.NETWORKDIA2)).map(s -> {
                            String[] item=s.split(NetworkCollection.NETWORKDIA);
                            return new WarehouseItem(item[0],Integer.parseInt(item[1]));
                        }).collect(Collectors.toList()));
                        break;
                    case W_DELIVERMATERIAL:
                        data=parameter.split(NetworkCollection.NETWORKDIA);
                        WarehouseConnector.warehouse_deliverMaterialRobot(Integer.parseInt(data[0]),data[1],Integer.parseInt(data[2]));
                        break;
                    default:
                        System.out.println("Not Supported Command: "+cmd+" -> "+parameter);
                }
                break;
        }
    }
    @SuppressWarnings("WeakerAccess")
    public void received(Subsystem subsystem, Command cmd){
        received(subsystem,cmd,null);
    }
    @SuppressWarnings("UnusedReturnValue")
    public boolean send(Subsystem subsystem, Command cmd){
        if(clients.get(subsystem)==null)
            return false;
        clients.get(subsystem).sendCommand(cmd);
        return true;
    }
    @SuppressWarnings("UnusedReturnValue")
    public boolean send(Subsystem subsystem, Command cmd, String parameter){
        if(clients.get(subsystem)==null)
            return false;
        clients.get(subsystem).sendCommand(cmd,parameter);
        return true;
    }
    @SuppressWarnings("WeakerAccess")
    public void addSubsystem(Subsystem subsystem, ServerClient serverClient){
        clients.put(subsystem,serverClient);
    }
    @SuppressWarnings("WeakerAccess")
    public void removeSubsystem(Subsystem subsystem) {
        clients.remove(subsystem);
    }
    public void addSubsystemListener(MapChangeListener<Subsystem,ServerClient> changeListener){
        clients.addListener(changeListener);
    }
    public void removeSubsystemListener(MapChangeListener<Subsystem,ServerClient> changeListener){
        clients.removeListener(changeListener);
    }
}
