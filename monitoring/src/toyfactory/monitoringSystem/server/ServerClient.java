package toyfactory.monitoringSystem.server;

import toyfactory.network.Client;
import toyfactory.network.data.Command;
import toyfactory.network.data.Subsystem;

import java.io.IOException;
import java.net.Socket;

public class ServerClient extends Client {
    private Subsystem subsystem;
    private Server server;
    @SuppressWarnings("WeakerAccess")
    public ServerClient(Socket socket, Server server) throws IOException {
        super(socket);
        this.server=server;
    }
    @Override
    public Subsystem getSubsystem(){
        return Subsystem.MonitoringSystem;
    }
    @Override
    public void connectionLost() {
        server.removeSubsystem(subsystem);
        super.close();
    }
    @Override
    public void receivedCommand(Command cmd) {
        if(cmd==Command.CLOSE) {
            close();
            server.removeSubsystem(subsystem);
        }
        server.received(subsystem,cmd);
    }
    @Override
    public void receivedCommand(Command cmd, String params) {
        System.out.println(cmd);
        if(cmd==Command.CONNECTED){
            subsystem=Subsystem.valueOf(params);
            server.addSubsystem(subsystem,this);
        }else{
            server.received(subsystem,cmd,params);
        }
    }
}
