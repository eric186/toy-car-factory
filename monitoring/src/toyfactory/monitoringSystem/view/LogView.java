package toyfactory.monitoringSystem.view;

import javafx.scene.control.TextArea;
import toyfactory.monitoringSystem.model.logger.Logger;
import toyfactory.network.data.Subsystem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

public class LogView extends TextArea {
    private ArrayList<Subsystem> aWhitelist;
    public LogView(Subsystem... whitelist){
        aWhitelist=Arrays.stream(whitelist).collect(Collectors.toCollection(ArrayList::new));
        if(aWhitelist.size()==0)
            aWhitelist=null;
        this.setEditable(false);
        Logger.addListener((subsystem, message) -> {
            boolean scroll=false;
            if(getCaretPosition()==getLength())
                scroll=true;
            if(aWhitelist==null){
                appendText(subsystem+": "+message+"\n");
            }else if(aWhitelist.contains(subsystem)){
                appendText(message+"\n");
            }
            if(scroll)
                positionCaret(getLength());
        });
    }
}
