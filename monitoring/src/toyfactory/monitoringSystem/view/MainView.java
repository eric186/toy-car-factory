package toyfactory.monitoringSystem.view;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.control.TabPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import toyfactory.monitoringSystem.model.Model;
import toyfactory.monitoringSystem.view.tabs.ConveyorSystemTab;
import toyfactory.monitoringSystem.view.tabs.MonitoringSystemTab;
import toyfactory.monitoringSystem.view.tabs.RobotSystemTab;
import toyfactory.monitoringSystem.view.tabs.WarehouseSystemTab;

import java.net.InetAddress;
import java.util.stream.Collectors;

public class MainView extends Application {
    private Model model;
    public MainView(){
        this.model=Model.model;
    }
    @Override
    public void start(Stage primaryStage){
        primaryStage.setTitle("Monitoring System");
        primaryStage.setScene(new Scene(initRoot(), 800, 600));
        primaryStage.show();
    }
    @SuppressWarnings("WeakerAccess")
    public VBox initRoot(){
        VBox root=new VBox();
        ListView<String> ipList=new ListView<>(FXCollections.observableArrayList(model.server.getIP().stream().map(InetAddress::getHostAddress).collect(Collectors.toList())));
        root.getChildren().add(ipList);
        ipList.setPrefHeight(50);
        TabPane tabPane=new TabPane();
        tabPane.getTabs().addAll(new MonitoringSystemTab(),new RobotSystemTab(),new ConveyorSystemTab(),new WarehouseSystemTab());
        root.getChildren().add(tabPane);
        return root;
    }
}
