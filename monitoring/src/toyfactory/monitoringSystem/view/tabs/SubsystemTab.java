package toyfactory.monitoringSystem.view.tabs;

import javafx.application.Platform;
import javafx.scene.control.Tab;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import toyfactory.monitoringSystem.model.Model;
import toyfactory.network.data.Subsystem;

@SuppressWarnings("WeakerAccess")
public class SubsystemTab extends Tab {
    public SubsystemTab(Subsystem subsystem){
        this.setText(subsystem.name());
        Model.model.addSystemAvailableListener(change -> {
            if(change.getKey()==subsystem){
                Platform.runLater(()->setState(change.getValueAdded()));
            }
        });
        setState(false);
        closableProperty().setValue(false);
    }
    protected void setState(boolean available){
        setGraphic(new Circle(8,available?Color.GREEN:Color.RED));
    }
}
