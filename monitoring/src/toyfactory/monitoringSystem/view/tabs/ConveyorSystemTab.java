package toyfactory.monitoringSystem.view.tabs;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import toyfactory.monitoringSystem.model.Model;
import toyfactory.monitoringSystem.model.connectors.ConveyorConnector;
import toyfactory.monitoringSystem.view.LogView;
import toyfactory.network.data.Subsystem;

import java.util.List;

public class ConveyorSystemTab extends SubsystemTab {
    private ObservableList<ConveyorConnector.ConveyorSpeed> speedList;
    private Button refresh;
    public ConveyorSystemTab() {
        super(Subsystem.Conveyor);
        speedList=FXCollections.observableArrayList();

        //getConveyorSpeed()

        HBox root=new HBox();
        root.setPadding(new Insets(5,5,5,5));
        VBox left=new VBox();

        TextField conveyor=new TextField();
        conveyor.textProperty().addListener((observable, oldValue, newValue) -> {
            if(!newValue.matches("[0-9]*"))
                conveyor.setText(oldValue);
        });
        conveyor.setPromptText("ID of Conveyor");
        ComboBox<Double> speed=new ComboBox<>(FXCollections.observableArrayList(0d,0.5,1d,1.5,2d,2.5,3d));
        speed.getSelectionModel().select(0);
        Button btn=new Button("SET SPEED");
        btn.setOnAction(event -> Model.model.setSpeedOfConveyor(Integer.parseInt(conveyor.getText()),speed.getValue()));

        refresh=new Button("REFRESH");
        refresh.setOnAction(event -> fillList());

        ListView<ConveyorConnector.ConveyorSpeed> speedListView=new ListView<>(speedList);
        speedListView.setPrefHeight(10000);
        HBox top=new HBox(conveyor,speed,btn);
        top.setAlignment(Pos.TOP_CENTER);
        top.setSpacing(5);
        left.setSpacing(5);
        left.getChildren().addAll(top,speedListView);
        LogView lw=new LogView(Subsystem.Conveyor);
        lw.setPrefHeight(10000);
        HBox.setHgrow(lw,Priority.ALWAYS);
        lw.prefWidthProperty().bind(root.widthProperty().subtract(10).divide(2));
        root.setSpacing(5);
        root.getChildren().addAll(left,lw);
        setContent(root);
    }

    private void fillList() {
        Thread d=new Thread(()->{
            Platform.runLater(()->refresh.setDisable(true));
            speedList.clear();
            System.out.println("start get stock");
            List<ConveyorConnector.ConveyorSpeed> items=Model.model.getConveyorSpeed();
            Platform.runLater(()->speedList.addAll(items));
            Platform.runLater(()->refresh.setDisable(false));
        });
        d.setDaemon(true);
        d.start();
    }

}
