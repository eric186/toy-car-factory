package toyfactory.monitoringSystem.view.tabs;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;
import toyfactory.monitoringSystem.model.Model;
import toyfactory.monitoringSystem.view.LogView;
import toyfactory.network.data.Subsystem;

public class MonitoringSystemTab extends SubsystemTab {
    private Thread emergencyThread;
    private Thread convoyerThread;
    public MonitoringSystemTab() {
        super(Subsystem.MonitoringSystem);
        this.setContent(init());
        setState(true);
    }

    private Node init(){
        VBox root=new VBox();
        root.setPadding(new Insets(5,5,5,5));
        HBox conveyorWarnings=new HBox();
        Polygon triangleEmergency=new Polygon(0.0,30.0,20,0,40,30);
        triangleEmergency.setStroke(Color.RED);
        triangleEmergency.setStrokeWidth(2);
        triangleEmergency.setFill(Color.color(0,0,0,0));
        conveyorWarnings.getChildren().add(triangleEmergency);
        Label emerg=new Label("EMERGENCY AT CONVEYOR");
        emerg.setAlignment(Pos.CENTER);
        conveyorWarnings.getChildren().add(emerg);

        Polygon triangleLubricant=new Polygon(0.0,30.0,20,0,40,30);
        triangleLubricant.setStroke(Color.YELLOW);
        triangleLubricant.setStrokeWidth(2);
        triangleLubricant.setFill(Color.color(0,0,0,0));
        conveyorWarnings.getChildren().add(triangleLubricant);
        Label lub=new Label("LOW LUBRICANT AT CONVEYOR: ");
        lub.setAlignment(Pos.CENTER);
        conveyorWarnings.getChildren().add(lub);
        Label lubConveyor=new Label("-");
        lubConveyor.setAlignment(Pos.CENTER);
        conveyorWarnings.getChildren().add(lubConveyor);

        Model.model.conveyorEmergencyProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue){
                if(emergencyThread==null) {
                    emergencyThread = new Thread(() -> {
                        while(!Thread.interrupted()){
                            try {
                                Thread.sleep(1000);
                            }catch(InterruptedException ignored){}
                            Platform.runLater(()->triangleEmergency.setFill(Color.RED));
                            try {
                                Thread.sleep(1000);
                            }catch(InterruptedException ignored){}
                            Platform.runLater(()->triangleEmergency.setFill(Color.color(0,0,0,0)));
                        }
                    });
                    emergencyThread.setDaemon(true);
                    emergencyThread.start();
                }
            }else{
                emergencyThread.interrupt();
                emergencyThread=null;
                Platform.runLater(()->triangleEmergency.setFill(Color.color(0,0,0,0)));
            }
        });
        Model.model.conveyorLubricantProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue.intValue()!=-1){
                Platform.runLater(()->lubConveyor.setText(""+newValue.intValue()));
                if(convoyerThread==null) {
                    convoyerThread = new Thread(() -> {
                        while(!Thread.interrupted()){
                            try {
                                Thread.sleep(1000);
                            }catch(InterruptedException ignored){}
                            Platform.runLater(()->triangleLubricant.setFill(Color.YELLOW));
                            try {
                                Thread.sleep(1000);
                            }catch(InterruptedException ignored){}
                            Platform.runLater(()->triangleLubricant.setFill(Color.color(0,0,0,0)));
                        }
                    });
                    convoyerThread.setDaemon(true);
                    convoyerThread.start();
                }
            }else{
                Platform.runLater(()->lubConveyor.setText("-"));
                convoyerThread.interrupt();
                convoyerThread=null;
                Platform.runLater(()->triangleLubricant.setFill(Color.color(0,0,0,0)));
            }
        });
        conveyorWarnings.setAlignment(Pos.TOP_CENTER);
        LogView logview=new LogView();
        root.setSpacing(5);
        logview.setPrefHeight(2000);
        root.getChildren().addAll(conveyorWarnings,logview);
        return root;
    }
}
