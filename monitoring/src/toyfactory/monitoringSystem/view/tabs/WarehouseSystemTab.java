package toyfactory.monitoringSystem.view.tabs;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import toyfactory.monitoringSystem.model.Model;
import toyfactory.monitoringSystem.model.structures.WarehouseItem;
import toyfactory.monitoringSystem.view.LogView;
import toyfactory.network.data.Subsystem;

import java.util.List;

public class WarehouseSystemTab extends SubsystemTab {
    private ObservableList<WarehouseItem> stockList;
    private Button refreshButton;
    public WarehouseSystemTab() {
        super(Subsystem.Warehouse);
        stockList=FXCollections.observableArrayList();
        HBox root=new HBox();
        root.setPadding(new Insets(5,5,5,5));
        VBox left=new VBox();
        TextField addItemName=new TextField();
        addItemName.setPromptText("Name of Item");
        TextField addItemCount=new TextField();
        addItemCount.setPromptText("Item count");
        addItemCount.textProperty().addListener((observable, oldValue, newValue) -> {
            if(!newValue.matches("[0-9]*"))
                addItemCount.setText(oldValue);
        });
        Button addButton=new Button("ADD");
        addButton.setOnAction(event -> Model.model.sendMaterialToWarehouse(addItemName.getText(),Integer.parseInt(addItemCount.getText())));

        refreshButton=new Button("REFRESH LIST");
        refreshButton.setOnAction(event -> fillList());
        Region spacer=new Region();
        HBox.setHgrow(spacer,Priority.ALWAYS);
        HBox top=new HBox(addItemName,addItemCount,addButton,spacer,refreshButton);
        top.setAlignment(Pos.TOP_CENTER);
        top.setSpacing(5);

        ListView<WarehouseItem> warehouseItemListView=new ListView<>(stockList);
        warehouseItemListView.setPrefHeight(2000);
        left.setSpacing(5);
        left.getChildren().addAll(top,warehouseItemListView);
        LogView lw=new LogView(Subsystem.Warehouse);
        lw.setPrefHeight(10000);
        HBox.setHgrow(lw,Priority.ALWAYS);
        lw.prefWidthProperty().bind(root.widthProperty().subtract(10).divide(2.5));
        root.setSpacing(5);
        root.getChildren().addAll(left,lw);
        setContent(root);
    }

    private void fillList() {
        Thread d=new Thread(()->{
            Platform.runLater(()->refreshButton.setDisable(true));
            stockList.clear();
            System.out.println("start get stock");
            List<WarehouseItem> items=Model.model.getStock();
            Platform.runLater(()->stockList.addAll(items));
            Platform.runLater(()->refreshButton.setDisable(false));
        });
        d.setDaemon(true);
        d.start();
    }
}
