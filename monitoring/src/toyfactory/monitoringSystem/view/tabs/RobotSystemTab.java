package toyfactory.monitoringSystem.view.tabs;

import javafx.geometry.Insets;
import javafx.scene.layout.VBox;
import toyfactory.monitoringSystem.view.LogView;
import toyfactory.network.data.Subsystem;

public class RobotSystemTab extends SubsystemTab {
    public RobotSystemTab() {
        super(Subsystem.Robot);
        VBox root=new VBox();
        root.setPadding(new Insets(5,5,5,5));
        LogView lw=new LogView(Subsystem.Robot);
        lw.setPrefHeight(10000);
        lw.setPrefWidth(10000);
        root.getChildren().add(lw);
        this.setContent(root);
    }
}
