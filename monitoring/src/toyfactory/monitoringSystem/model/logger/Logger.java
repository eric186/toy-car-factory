package toyfactory.monitoringSystem.model.logger;

import toyfactory.network.data.Subsystem;

import java.util.ArrayList;

public class Logger {
    private static ArrayList<LogListener> logListeners=new ArrayList<>();
    public static void sendMessage(Subsystem subsystem,String message){
        logListeners.forEach(l->l.receviedMessage(subsystem,message));
    }
    public static void addListener(LogListener listener){
        logListeners.add(listener);
    }
    @SuppressWarnings("unused")
    public static void removeListener(LogListener listener){
        logListeners.remove(listener);
    }
    public interface LogListener {
        void receviedMessage(Subsystem subsystem,String message);
    }
}
