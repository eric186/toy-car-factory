package toyfactory.monitoringSystem.model;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.FXCollections;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableMap;
import toyfactory.monitoringSystem.model.connectors.ConveyorConnector;
import toyfactory.monitoringSystem.model.connectors.WarehouseConnector;
import toyfactory.monitoringSystem.model.structures.WarehouseItem;
import toyfactory.monitoringSystem.server.Server;
import toyfactory.network.data.NetworkCollection;
import toyfactory.network.data.Subsystem;

import java.util.Arrays;
import java.util.List;

public class Model{
    public static Model model;
    public Server server;
    private ObservableMap<Subsystem,Boolean> systemIsAvailabe;
    private SimpleBooleanProperty conveyorEmergency;
    private SimpleIntegerProperty conveyorLubricant;
    public Model(){
        model=this;
        conveyorEmergency=new SimpleBooleanProperty(false);
        conveyorLubricant=new SimpleIntegerProperty(-1);
        server=new Server(NetworkCollection.PORT);
        systemIsAvailabe= FXCollections.observableHashMap();
        Arrays.stream(Subsystem.values()).filter(s->s!=Subsystem.MonitoringSystem).forEach(s->systemIsAvailabe.put(s,false));
        server.addSubsystemListener(change -> {
            if(change.wasAdded()) {
                systemIsAvailabe.put(change.getKey(),true);
            }
            if(change.wasRemoved()) {
                systemIsAvailabe.put(change.getKey(),false);
            }
        });
    }
    public void addSystemAvailableListener(MapChangeListener<Subsystem,Boolean> listener){
        systemIsAvailabe.addListener(listener);
    }
    @SuppressWarnings("unused")
    public void removeSystemAvailableListener(MapChangeListener<Subsystem,Boolean> listener){
        systemIsAvailabe.removeListener(listener);
    }
    public boolean isSubsystemAvailable(Subsystem subsystem) {
        return systemIsAvailabe.get(subsystem);
    }
    public void triggerConveyorEmergency() {
        conveyorEmergency.set(true);
    }
    public void triggerConveyorNeedLubricant(int idConveyor){
        conveyorLubricant.set(idConveyor);
    }
    public List<WarehouseItem> getStock(){
        return WarehouseConnector.requestStock();
    }
    public List<ConveyorConnector.ConveyorSpeed> getConveyorSpeed(){
        return ConveyorConnector.getSpeedOfAllConvoyers();
    }
    public void setSpeedOfConveyor(int IDConveyor,double speed){
        ConveyorConnector.setSpeedOfConveyor(IDConveyor,speed);
    }
    public void sendMaterialToWarehouse(String name,int count){
        WarehouseConnector.materialImported(name,count);
    }
    public SimpleBooleanProperty conveyorEmergencyProperty() {
        return conveyorEmergency;
    }
    public SimpleIntegerProperty conveyorLubricantProperty() {
        return conveyorLubricant;
    }
}
