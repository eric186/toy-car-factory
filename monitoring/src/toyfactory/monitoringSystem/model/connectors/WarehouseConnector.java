package toyfactory.monitoringSystem.model.connectors;

import toyfactory.monitoringSystem.model.Model;
import toyfactory.monitoringSystem.model.logger.Logger;
import toyfactory.monitoringSystem.model.structures.WarehouseItem;
import toyfactory.network.data.Command;
import toyfactory.network.data.NetworkCollection;
import toyfactory.network.data.Subsystem;

import java.util.List;

public class WarehouseConnector {
    private static List<WarehouseItem> itemsLastRequest;
    public static void warehouse_deliverMaterialRobot(int IDRobot, String name, int count) {
        Logger.sendMessage(Subsystem.Warehouse,"Delivered Material: "+IDRobot+" <- "+name+": "+count);
        RobotConnector.robot_addMaterialRobot(IDRobot,name,count);
    }
    public static void warehouse_currentStock(List<WarehouseItem> currentStock) {
        Logger.sendMessage(Subsystem.Warehouse,"Successful stock request");
        itemsLastRequest=currentStock;
    }
    public static List<WarehouseItem> requestStock(){
        Logger.sendMessage(Subsystem.Warehouse,"Start stock request");
        itemsLastRequest=null;
        Model.model.server.send(Subsystem.Warehouse,Command.M_W_GETSTOCK);
        do{
            try{
                Thread.sleep(1000);
            }catch(InterruptedException ignored){}
        }while(!Thread.interrupted()&&Model.model.isSubsystemAvailable(Subsystem.Warehouse)&&itemsLastRequest==null);
        Logger.sendMessage(Subsystem.Warehouse,"Return stock request results");
        return itemsLastRequest;
    }
    @SuppressWarnings("WeakerAccess")
    public static void materialRequest(int IDRobot, String name, int count){
        Logger.sendMessage(Subsystem.Warehouse,"Material request from warehouse <- "+IDRobot+" -> "+name+": "+count);
        Model.model.server.send(Subsystem.Warehouse,Command.M_W_REQMATERIAL,IDRobot+NetworkCollection.NETWORKDIA+name+NetworkCollection.NETWORKDIA+count);
    }
    public static void materialImported(String name, int count){
        Logger.sendMessage(Subsystem.Warehouse,"Material imported to warehouse <- "+name+": "+count);
        Model.model.server.send(Subsystem.Warehouse,Command.M_W_IMPORT,name+NetworkCollection.NETWORKDIA+count);
    }
}
