package toyfactory.monitoringSystem.model.connectors;

import toyfactory.monitoringSystem.model.Model;
import toyfactory.monitoringSystem.model.logger.Logger;
import toyfactory.network.data.NetworkCollection;
import toyfactory.network.data.Subsystem;

import static toyfactory.network.data.Command.*;

public class RobotConnector {
    public static void robot_checkConveyorState(int IDRobot, int IDConveyor) {
        Logger.sendMessage(Subsystem.Robot,"Checkrequest for item at conveyor: "+IDConveyor+" by robot: "+IDRobot);
        Boolean result=ConveyorConnector.isItemAtConveyor(IDConveyor);
        if(result!=null&&result){
            robot_itemAvailableAtConveyor(IDRobot);
        }
    }
    public static void robot_deliverAtConveyor(int IDRobot,int IDConveyor) {
        Logger.sendMessage(Subsystem.Robot,"Item put at conveyor "+IDConveyor+" by robot "+IDRobot);
        ConveyorConnector.putItemAtConvoyer(IDConveyor);
    }
    public static void robot_requestMaterialWarehouse(int IDRobot,String name,int count) {
        Logger.sendMessage(Subsystem.Robot,"Request to warehouse by robot: "+IDRobot+" <- "+name+": "+count);
        WarehouseConnector.materialRequest(IDRobot,name,count);
    }
    public static void robot_takeOfConveyor(int IDRobot,int IDConveyor) {
        Logger.sendMessage(Subsystem.Robot,"Item taken from conveyor "+IDConveyor+" by robot "+IDRobot);
        ConveyorConnector.itemTakenFromConveyor(IDConveyor);
    }
    @SuppressWarnings("WeakerAccess")
    public static void robot_addMaterialRobot(int IDRobot, String name, int count){
        Logger.sendMessage(Subsystem.Robot,"Material given to robot: "+IDRobot+" <- "+name+": "+count);
        Model.model.server.send(Subsystem.Robot,M_R_ADDMATERIAL,IDRobot+NetworkCollection.NETWORKDIA+name+NetworkCollection.NETWORKDIA+count);
    }
    @SuppressWarnings("WeakerAccess")
    public static void robot_itemAvailableAtConveyor(int IDRobot){
        Logger.sendMessage(Subsystem.Robot,"Item is available at robot: "+IDRobot);
        Model.model.server.send(Subsystem.Robot,M_R_ITEMAT,""+IDRobot);
    }
}
