package toyfactory.monitoringSystem.model.connectors;

import toyfactory.monitoringSystem.model.Model;
import toyfactory.monitoringSystem.model.logger.Logger;
import toyfactory.network.data.Command;
import toyfactory.network.data.NetworkCollection;
import toyfactory.network.data.Subsystem;

import java.util.HashMap;
import java.util.List;

public class ConveyorConnector {
    private static HashMap<Integer,Boolean> checkItemAtConveyor=new HashMap<>();
    private static List<ConveyorSpeed> speedLastRequest;
    public static void conveyor_itemAvailable(int IDrobot){
        Logger.sendMessage(Subsystem.Conveyor,"Item available at robot: "+IDrobot);
        RobotConnector.robot_itemAvailableAtConveyor(IDrobot);
    }
    public static void conveyor_noItemAt(int IDConveyor){
        Logger.sendMessage(Subsystem.Robot,"No item at conveyor: "+IDConveyor);
        /* vayne */
    }
    public static void conveyor_emergencyPressed(){
        Logger.sendMessage(Subsystem.Robot,"Emergency triggered");
        Model.model.triggerConveyorEmergency();
    }
    public static void conveyor_needLubricant(int IDConveyor){
        Logger.sendMessage(Subsystem.Robot,"Need lubricant at conveyor: "+IDConveyor);
        Model.model.triggerConveyorNeedLubricant(IDConveyor);
    }
    public static void conveyor_speedOfConveyor(List<ConveyorSpeed> conveyorSpeeds){
        Logger.sendMessage(Subsystem.Robot,"Received list of conveyor speeds");
        speedLastRequest=conveyorSpeeds;
    }
    public static void conveyor_state(int IDConveyor, boolean isFull) {
        Logger.sendMessage(Subsystem.Robot,"Received state of conveyor: "+IDConveyor+" ISFULL: "+isFull);
        if(checkItemAtConveyor.containsKey(IDConveyor))
            checkItemAtConveyor.put(IDConveyor,isFull);
    }
    public static List<ConveyorSpeed> getSpeedOfAllConvoyers(){
        Logger.sendMessage(Subsystem.Robot,"Request speed of all conveyors");
        speedLastRequest=null;
        Model.model.server.send(Subsystem.Conveyor,Command.M_C_GETSPEED);
        do{
            try{
                Thread.sleep(1000);
            }catch(InterruptedException ignored){}
        }while(!Thread.interrupted()&&Model.model.isSubsystemAvailable(Subsystem.Conveyor)&&speedLastRequest==null);
        Logger.sendMessage(Subsystem.Robot,"Return list of conveyor speed");
        return speedLastRequest;
    }
    @SuppressWarnings("WeakerAccess")
    public static void putItemAtConvoyer(int IDConveyor){
        Logger.sendMessage(Subsystem.Robot,"Put item at conveyor: "+IDConveyor);
        putItemAtConvoyer(IDConveyor,3);
    }
    @SuppressWarnings("WeakerAccess")
    public static void putItemAtConvoyer(int IDConveyor, double speed){
        Logger.sendMessage(Subsystem.Robot,"Put item at conveyor: "+IDConveyor+" with speed: "+speed);
        Model.model.server.send(Subsystem.Conveyor,Command.M_C_ITEMPUT,IDConveyor+NetworkCollection.NETWORKDIA+speed);
    }
    public static void setSpeedOfConveyor(int IDConveyor, double speed){
        Logger.sendMessage(Subsystem.Robot,"Set speed of conveyor: "+IDConveyor+" to speed: "+speed);
        Model.model.server.send(Subsystem.Conveyor,Command.M_C_SETSPEED,IDConveyor+NetworkCollection.NETWORKDIA+speed);
    }
    @SuppressWarnings("WeakerAccess")
    public static Boolean isItemAtConveyor(int IDConveyor){
        Logger.sendMessage(Subsystem.Robot,"Request item at conveyor: "+IDConveyor);
        if(!checkItemAtConveyor.containsKey(IDConveyor)) {
            checkItemAtConveyor.put(IDConveyor,null);
            Model.model.server.send(Subsystem.Conveyor, Command.M_C_ISITEMAT, "" + IDConveyor);
            do{
                try{
                    Thread.sleep(100);
                }catch(InterruptedException ignored){}
            }while(!Thread.interrupted()&&Model.model.isSubsystemAvailable(Subsystem.Warehouse)&&checkItemAtConveyor.get(IDConveyor)!=null);
            Logger.sendMessage(Subsystem.Robot,"Return item at conveyour result: "+checkItemAtConveyor.get(IDConveyor));
            return checkItemAtConveyor.remove(IDConveyor);
        }
        Logger.sendMessage(Subsystem.Robot,"Request of item at conveyor failed");
        return null;
    }
    @SuppressWarnings("WeakerAccess")
    public static void itemTakenFromConveyor(int IDConveyor){
        Logger.sendMessage(Subsystem.Robot,"Take item of conveyor: "+IDConveyor);
        Model.model.server.send(Subsystem.Conveyor,Command.M_C_ITEMTAKE,""+IDConveyor);
    }
    @SuppressWarnings("WeakerAccess")
    public static class ConveyorSpeed{
        public int IDConveyor;
        public double speed;
        public ConveyorSpeed(int IDConveyor,double speed){
            this.IDConveyor=IDConveyor;
            this.speed=speed;
        }
        @Override
        public String toString(){
            return "Convoyer: "+IDConveyor+" -> "+speed;
        }
    }
}
